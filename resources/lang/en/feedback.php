<?php

return [
    "Write to us" => "Contact Us",
    "Full name" => "Full name",
    "Phone" => "Phone",
    "Message text" => "Message",
    "Send" => "Send",
    'Contact Us' => 'Contact Us',
];
