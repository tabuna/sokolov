<?php

return [
    'Services' => 'Our services',
    'Work' => 'How we work',
    'Advantages' => 'Our Advantages',
    'Order' => 'Order now',
    'News' => 'News and Promotions',
    'News-sub' => 'All news and events',
    'Reviews' => 'Customer Reviews',
    'Reviews-sub' => 'All reviews',
    'Learn More' => 'Learn more',
    'panel' => 'Account',
    'sign' => 'Log In',
    'dashboard' => 'Panel',
    'logout' => 'Log Out',
    'userhome' => 'Your Account',
    'editorhome' => 'Your Account',


    "We employ only experienced scientific academic editors who are experts in a particular scientific field. All our editors - experts in English grammar, spelling and punctuation." =>
        "We only employ experienced scientific editors who are experts in a particular scientific field. All our editors are experts in English grammar, spelling and punctuation.",

    "Our scientific editors have extensive experience in prestigious academic research institutions. Our editors have a significant number of highly cited publications in scientific journals in English." =>
        "Our scientific editors have extensive experience working in prestigious academic research institutions and have a significant number of highly cited publications in scientific journals in English.",

    "Our scientific editors wrote, edited and reviewed scientific publications and grants." =>
        "Our scientific editors have authored, edited and reviewed scientific publications and grants.",

    "We know that you need to publish your important scientific discoveries in the English-language journal. The language barrier should never be a barrier to the spread of important ideas and discoveries! We are here to help you succeed!" =>
        "We know what is required for you to publish your important scientific discoveries in an English language journal. The language barrier should never be an obstacle to dissemination of important ideas and discoveries! We are here to help you succeed!",

    "If your research is not published - they never had. Many manuscripts are accepted for publication because of the poor quality of the English language. Our editors - experts in English. Our editors - scientific experts with many years of research and publications in prestigious scientific journals." =>
        "Many manuscripts are rejected for publication because of poor quality of English. Our editors are experts in English, hold advanced Ph.D. degrees in sciences from prestigious United States universities, have many years of research experience and have published in peer-reviewed scientific journals",

    "We guarantee that your paper will not be rejected by the scientific journal of the poor quality of the English language. We re-edit your article - free of charge."
    => "We guarantee that your manuscript will not be rejected by your target scientific journal because of poor quality of English. We will re-edit your article - free of charge!",


    "You" => "You",
    "We" => "We",


    "Editing" => "Editing",
    "Correcting written
                             English scientific manuscripts
                             and elimination of grammar and
                             spelling mistakes" => "Highlight the uniqueness of your work with editing performed by English experts with advanced Ph.D. degrees",


    "Translation" => "Translation",
    "We provide technical
                             translation from Russian into
                             English" => "Competent and prompt technical translations of scientific manuscripts and other documents from Russian to English",

    "Format" => "Formatting",


    "Fit manuscripts under strict
                             foreign scientific standards
                             magazines" => "Identification and correction of formatting that does not meet the guidelines of your target journal",
    "illustrating" => "Illustration",
    "Creation of scientific graphics
                             according to the illustrations
                             provided sketch or
                             description" => "Modification and creation of unique figures, explanatory graphs and tables that help your manuscript stand out",


    "Order services" => "Order the service",
    "To pay for services" => "Pay for the service",
    "You get ready to work" => "Receive the completed work",
    "We expect the price" => "Issue the invoice",
    "To provide services" => "Provide the service",
    "Get Your review" => "Receive your review",


    "Search" => "Search",
];
