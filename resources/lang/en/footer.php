<?php

return [
    'mission' => 'Our mission',
    'about' => 'To strengthen the reputations of scholars from different countries by helping you publish your research in English journals.',
    'contacts' => 'Contacts',
    'navigation' => 'Website Navigation',
    'payment' => 'Payment Methods',
    'octavian' => 'Website development, support and promotion',
    'Denis A. Sokolov' => 'Denis A. Sokolov',
];
