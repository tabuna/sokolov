<?php

return [
    'Contact Us' => 'Контакты',
    "Write to us" => "Написать нам",
    "Full name" => "ФИО",
    "Phone" => "Телефон",
    "Message text" => "Текст сообщения",
    "Send" => "Отправить",
];
