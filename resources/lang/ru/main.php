<?php

return [
    'Services' => 'Наши услуги',
    'Work' => 'Как мы работаем',
    'Advantages' => 'Наши преимущества',
    'Order' => 'Заказать услугу',
    'News' => 'Новости и Акции',
    'News-sub' => 'Все новости и акции',
    'Reviews' => 'Отзывы наших клиентов',
    'Reviews-sub' => 'Все отзывы',
    'Learn More' => 'Узнать больше',
    'panel' => 'Сервис',
    'sign' => 'Вход',
    'dashboard' => 'Панель',
    'logout' => 'Выход',
    'userhome' => 'Личный кабинет',
    'editorhome' => 'Личный кабинет',

    "We employ only experienced scientific academic editors who are experts in a particular scientific field. All our editors - experts in English grammar, spelling and punctuation." => "У нас работают только опытные научные академические редакторы, которые являются экспертами в конкретной научной области. Все наши редакторы – эксперты в английской грамматике, орфографии и пунктуации.",
    "Our scientific editors have extensive experience in prestigious academic research institutions. Our editors have a significant number of highly cited publications in scientific journals in English." => "Наши научные редакторы имеют значительный опыт работы в престижных академических научных исследовательских
учреждениях. Наши редакторы имеют значительное количество публикации в высоко цитируемых научных журналах на
английском языке.",
    "Our scientific editors wrote, edited and reviewed scientific publications and grants." => "Наши научные редакторы являются авторами, редакторами и рецензентами научных публикаций и грантов.",
    "We know that you need to publish your important scientific discoveries in the English-language journal. The language barrier should never be a barrier to the spread of important ideas and discoveries! We are here to help you succeed!" => "Мы знаем, что необходимо для публикации ваших важных научных открытий в англоязычном научном журнале. Языковой барьер никогда не должен быть препятствием для распространения важных идей и открытий! Мы здесь, чтобы помочь вам добиться успеха!",
    "If your research is not published - they never had. Many manuscripts are accepted for publication because of the poor quality of the English language. Our editors - experts in English. Our editors - scientific experts with many years of research and publications in prestigious scientific journals." => "Если ваши исследования не опубликованы – их никогда не было. Множество рукописей не принимаются к публикации из-за плохого качества английского языка. Наши редакторы – эксперты английского языка. Наши редакторы – научные эксперты с многими годами исследовательской деятельности и публикациями в престижных научных журналах.",
    "We guarantee that your paper will not be rejected by the scientific journal of the poor quality of the English language. We re-edit your article - free of charge." => "Мы гарантируем, что ваша статья не будет отвергнута научным журналом из-за низкого качества английского языка. Мы повторно отредактируем вашу статью – бесплатно.",


    "You" => "Вы",
    "We" => "Мы",


    "Editing" => "Редактирование",
    "Correcting written
                             English scientific manuscripts
                             and elimination of grammar and
                             spelling mistakes" => "Подчеркните уникальность вашей работы, доверяя корректировку английского языка профессиональными редакторам с ученой степенью кандидата наук и выше",


    "Translation" => "Перевод",
    "We provide technical
                             translation from Russian into
                             English" => "Грамотный и оперативный технический перевод рукописей и других документов с русского на английский",

    "Format" => "Форматирование",


    "Fit manuscripts under strict
                             foreign scientific standards
                             magazines" => "Выявление и исправление проблем c форматированием, не соответствующему требованиям Вашего целевого зарубежного издания",
    "illustrating" => "Иллюстрирование",

    "Creation of scientific graphics
                             according to the illustrations
                             provided sketch or
                             description" => "Изменение и создание уникальных и интересных иллюстраций, поясняющих графиков и таблиц, которые помогут вашей рукописи выделиться",


    "Order services" => "Заказываете Услугу",
    "To pay for services" => "Оплачиваете Услугу",
    "You get ready to work" => "Получаете готовую работу",
    "We expect the price" => "Рассчитываем стоимость",
    "To provide services" => "Оказываем Услугу",
    "Get Your review" => "Получаем Ваш отзыв",


    "Search" => "Поиск",
];
