<?php

return [

    "service" => "СЕРВИС",
    "create order" => "Создать заказ",
    "orders" => "Заказы",
    "invoice" => "Счета",
    "control" => "Управление",
    "settings" => "Настроки",
    "exit" => "Выйти",
    "Hello" => "Здравствуйте",

    "feed task" => "Лента задач",
    "task" => "Задачи",
    "TASK HISTORY" => "ВЫПОЛНЕННЫЕ ЗАДАНИЯ",
];
